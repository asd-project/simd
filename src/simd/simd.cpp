//---------------------------------------------------------------------------

#include <simd/simd.h>
#include <limits>

//---------------------------------------------------------------------------

#ifdef __GNUC__
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wignored-attributes"
#endif

namespace asd
{
	template <> const byte	  simd_zero<byte>		::value = 0;
	template <> const int	  simd_zero<int>		::value = 0;
	template <> const int64	  simd_zero<int64>		::value = 0;
	template <> const float    simd_zero<float>		::value = 0.0f;

	template <> const float    simd_max<float>		::value = simd_data<float, 4>::get<0>(simd_data<float, 4>(_mm_castsi128_ps(_mm_set1_epi32(0xFFFF'FFFF))));
	template <> const byte	  simd_max<byte>		::value = 0xFF;
	template <> const int	  simd_max<int>			::value = 0xFFFF'FFFF;
	template <> const int64	  simd_max<int64>		::value = 0xFFFF'FFFF'FFFF'FFFF;

	template <> const byte     simd_sign_mask<byte>	::value = 0x80;
	template <> const int      simd_sign_mask<int>	::value = 0x8000'0000;
	template <> const int64    simd_sign_mask<int64>	::value = 0x8000'0000'0000'0000;
	template <> const float    simd_sign_mask<float>	::value = simd_data<float, 4>::get<0>(simd_data<float, 4>(_mm_castsi128_ps(_mm_set1_epi32(0x8000'0000))));

	template <> const byte	  simd_no_frac<byte>		::value = 0;
	template <> const int	  simd_no_frac<int>		::value = 0;
	template <> const int64	  simd_no_frac<int64>	::value = 0;
	template <> const float    simd_no_frac<float>	::value = 8388608.0f; // float(0x80'0000)

	const simd<int, 4>   ::inner simd<int, 4>   ::maximum  = simd_mask<int,    simd_max,      mk_mask4(1, 1, 1, 1)>::get();
	const simd<int, 4>   ::inner simd<int, 4>   ::signmask = simd_mask<int,    simd_sign_mask, mk_mask4(1, 1, 1, 1)>::get();
	const simd<float, 4> ::inner simd<float, 4> ::signmask = simd_mask<float,  simd_sign_mask, mk_mask4(1, 1, 1, 1)>::get();
	const simd<float, 4> ::inner simd<float, 4> ::nofrac   = simd_mask<float,  simd_no_frac,   mk_mask4(1, 1, 1, 1)>::get();

#ifdef USE_AVX
	template <> const double simd_zero<double>		::value = 0.0;
	template <> const double simd_max<double>		::value = simd_data<double, 4>::get<0>(simd_data<double, 4>(_mm256_castsi256_pd(_mm256_set1_epi64x(0xFFFF'FFFF'FFFF'FFFF))));
	template <> const double simd_sign_mask<double>	::value = simd_data<double, 4>::get<0>(simd_data<double, 4>(_mm256_castsi256_pd(_mm256_set1_epi64x(0x8000'0000'0000'0000))));
	template <> const double simd_no_frac<double>	::value = 36028797018963968.0; // double(0x80'0000'0000'0000)
	const simd<double, 4>::inner simd<double, 4>::signmask = simd_mask<double, simd_sign_mask, mk_mask4(1, 1, 1, 1)>::get();
	const simd<double, 4>::inner simd<double, 4>::nofrac   = simd_mask<double, simd_no_frac,   mk_mask4(1, 1, 1, 1)>::get();
#endif
}

#ifdef __GNUC__
#pragma GCC diagnostic pop
#endif
